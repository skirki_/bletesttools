package info.androidhive.systemevents;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import info.androidhive.applicationpipes.TelephonyEventsReceiver;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;

/**
 * PhoneIntentReceiver is a observer and TelephonyEventsReceivers are receivers
 *
 * to add new receiver use 'setReceiver' method
 * to remove and stop getting notification use 'removeReceiver'
 */
public class PhoneIntentReceiver extends BroadcastReceiver {

    Context context = null;
    private static final String TAG = "Phone call";
    private ITelephony telephonyService;

    static List<TelephonyEventsReceiver> activityReceivers = new ArrayList<>();

    private static final Integer END_POINT_ACTION_END_CALL = 1;
    private static final Integer END_POINT_ACTION_MUTE     = 2;
    private static final Integer END_POINT_ACTION_VIBRATION= 3;
    private static final Integer END_POINT_ACTION_NORMAL   = 3;

    private static TelephonyManager telephony;

    private static Boolean active = false;

    public void setActive(){
        active = true;
    }

    private void deactivate(){
        active = false;
    }

    public void disable(){
        active = false;
    }

    public void setReceiver(TelephonyEventsReceiver receiver){
        activityReceivers.add(receiver);
        setActive();
    }

    public void removeReceiver(TelephonyEventsReceiver receiver){
        activityReceivers.remove(receiver);

        if(activityReceivers.isEmpty()){
            deactivate();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "Receving....");

        if(!active) return;

        telephony = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);

        Bundle bundle = intent.getExtras();
        String phoneState = bundle.getString("state");
        String phoneNumber = bundle.getString("incoming_number");

        Log.v(TAG, "Phone state: " + phoneState);
        Log.v(TAG, "Phone number: " + phoneNumber);


        for(TelephonyEventsReceiver r : activityReceivers) {
            r.incomingCallEventReceive(new TelephonyWrapper(phoneNumber, phoneState));
        }
    }


    public void recall(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);

        intent.setData(Uri.parse("tel:" + phoneNumber));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void endCall(){
        phoneStateEndPoint(telephony, END_POINT_ACTION_END_CALL);
    }

    public void rejectWithMessage(String rejectMsg){

    }

    private void endCall(TelephonyManager telephony){
        phoneStateEndPoint(telephony, END_POINT_ACTION_END_CALL);
    }

    private void silenceRinger(TelephonyManager telephony){
        phoneStateEndPoint(telephony, END_POINT_ACTION_MUTE);
    }

    private void setVibrationRinger(TelephonyManager telephony){
        phoneStateEndPoint(telephony, END_POINT_ACTION_MUTE);
    }

    private void phoneStateEndPoint(TelephonyManager telephony, Integer endPointAction) {

        for (TelephonyEventsReceiver r : activityReceivers) {
            try {
                AudioManager am = (AudioManager) r.getContext().getSystemService(Context.AUDIO_SERVICE);

                Class c = Class.forName(telephony.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                telephonyService = (ITelephony) m.invoke(telephony);

                if (endPointAction.equals(END_POINT_ACTION_END_CALL)) {
                    //reject call
                    telephonyService.endCall();
                } else if (endPointAction.equals(END_POINT_ACTION_MUTE)) {
                    //For Silent mode
                    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                } else if (endPointAction.equals(END_POINT_ACTION_NORMAL)) {
                    //For Normal mode
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                } else if (endPointAction.equals(END_POINT_ACTION_VIBRATION)) {
                    //For Vibrate mode
                    am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
