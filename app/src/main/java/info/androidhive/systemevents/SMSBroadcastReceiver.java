package info.androidhive.systemevents;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.applicationpipes.TelephonyEventsReceiver;
import info.androidhive.applicationpipes.result.SmsEventResult;
import info.androidhive.applicationpipes.wrappers.SmsEventWrapper;

public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SMSBroadcastReceiver";

    private static TelephonyEventsReceiver activityReceiver;

    public void setActivity(TelephonyEventsReceiver  receiver){
        activityReceiver = receiver;
        setActive();
    }

    private static Boolean active = false;

    public static void setActive(){
        active = true;
    }

    public static void disable(){
        active = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent recieved: " + intent.getAction());

        if(!active) return;

        if (intent.getAction() == SMS_RECEIVED) {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            List<SmsMessage> smses = new ArrayList<SmsMessage>();
            if (bundle != null) {
                Object[] pdus = (Object[])bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                String originalAddress;
                String tmpSmsBody;
                for (int i=0; i<msgs.length; i++){
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);

                    smses.add(msgs[i]);
                    // Original Address
                    originalAddress = msgs[i].getOriginatingAddress();

                    // Message body
                    tmpSmsBody = msgs[i].getMessageBody().toString();
                }

                SmsEventResult result = activityReceiver.smsEventReceive(new SmsEventWrapper(smses) );

                if(result==null) return;
            }
        }
    }

    public void sendSMS(String phoneNumber, String message)
    {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }
}