package info.androidhive.communication;

/**
 * Created by hjacker on 17.05.15.
 */
public class BleServiceIPC_Messages {

    // Message.what is a User-defined message code so
    // that the recipient can identify what the message is about.
    public static final int MSG_BIND_SERVICE = 1;

    /**
     * Make Bluetooth LE SCAN
     */
    public static final int MSG_SCAN_BLE_DEVICES = 2;

    /**
     * Please make connection with device
     */
    public static final int MSG_CONNECT_TO_DEVICE = 3;

    /**
     * Write Characteristic to connected device
     */
    public static final int MSG_WRITE_CHARACTERISTIC_TO_DEVICE = 4;

    /**
     * Read Characteristic from connected device
     */
    public static final int MSG_READ_CHARACTERISTIC_FROM_DEVICE = 5;


    /**
     * Disconnect from device
     */
    public static final int MSG_DISCONNECT_FROM_DEVICE = 11;

    /**
     * Get RSSI value
     */
    public static final int MSG_UPDATE_TARGET_RSSI = 12;

    /**
     * Objects codes
     */
    public static final String OBJ_BLE_DEVICES_NEARBY = "info.androidhive.communication.obj_ble_devices_nearby";

    public static final String RESULT = "info.androidhive.ble.result";
    public static final String ACTION = "info.androidhive.ble.action";
    public static final String OBJECT = "info.androidhive.ble.object";
    public static final String NOTIFICATION_CODE = "info.androidhive.functional";
    public static final String NOTIFICATION = "info.androidhive.functional";


    public static final String MAC_ADDRESS = "info.androidhive.mac_address";
    public static final String BYTE_ARRAY = "info.androidhive.ui_payload";
    public static final String RSSI = "info.androidhive.rssi";
}
