package info.androidhive.actionbar;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

public class Action3Activity extends Activity {

    private static Context context;
    private boolean mIsBound = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action3);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);
        context = this;

        configurePushButtons();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void configurePushButtons() {

    }

}
