package info.androidhive.actionbar;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import info.androidhive.actionbar.lock.MyAdmin;
import info.androidhive.applicationpipes.wrappers.TelephonyWrapper;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.systemevents.PhoneIntentReceiver;
import info.androidhive.systemevents.SMSBroadcastReceiver;
import info.androidhive.telephony.TelephonyService;

public class Action8Activity extends AbstractCommunicationActivity {

    static final int RESULT_ENABLE = 1;

    private static Context context;
    private boolean mIsBound = false;
    ComponentName compName;

    private String callerNumber;

    private static boolean callStatus = false;

    public PersonalData pd = new PersonalData();

    protected PhoneIntentReceiver phoneIntentReceiver;
    protected SMSBroadcastReceiver smsBroadcastReceiver;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action8);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);
        context = this;
        compName = new ComponentName(this, MyAdmin.class);

        configurePushButtons();

        Intent intent = new Intent(this, TelephonyService.class);
        bindService(intent, teleServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void configurePushButtons(){
        Button save = (Button) findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emergencyNumber = "";
                String emergencyMsg = "";
                String rejectMsg = "";

                EditText emergencyNumberText = (EditText) findViewById(R.id.editText);
                EditText emergencyMsgText = (EditText) findViewById(R.id.editText2);
                EditText rejectMsgText = (EditText) findViewById(R.id.editText3);

                if (emergencyNumberText.getText() != null) {
                    emergencyNumber = emergencyNumberText.getText().toString();
                    pd.setEmergencyNumber(emergencyNumber);
                }

                if (emergencyMsgText.getText() != null) {
                    emergencyMsg = emergencyMsgText.getText().toString();
                    pd.setEmergencyMsg(emergencyMsg);
                }

                if (rejectMsgText.getText() != null) {
                    rejectMsg = rejectMsgText.getText().toString();
                    pd.setRejectMsg(rejectMsg);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived(byte [] f) {
        if(callStatus) {
            phoneIntentReceiver.endCall();
            callStatus = false;
            if (callerNumber != null) {

                if (PersonalData.getRejectMsg() == null)
                    smsBroadcastReceiver.sendSMS(callerNumber, "I will call You later");
                else
                    smsBroadcastReceiver.sendSMS(callerNumber, PersonalData.getRejectMsg());
            }
        } else {
            if(PersonalData.getEmergencyNumber()  != null ){
                smsBroadcastReceiver.sendSMS(PersonalData.getEmergencyNumber(), PersonalData.getEmergencyMsg());
            }
        }
    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {

    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

    private void onCallStart(TelephonyWrapper telephone){
        callStatus = true;
        callerNumber = telephone.getPhoneNumber();
    }

    private void onCallEnd(TelephonyWrapper telephone){
        callStatus = false;
        callerNumber = null;
    }

    Messenger teleMessenger;
    Boolean isBound;

    private ServiceConnection teleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            teleMessenger = new Messenger(service);

            // Create a Message
            // Note the usage of MSG_SAY_HELLO as the what value
            Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_BIND_SERVICE, 0, 0);

            // Create a bundle with the data
            Bundle bundle = new Bundle();
            bundle.putString("hello", "BleService bound");

            // Set the bundle data to the Message
            msg.setData(bundle);

            // Send the Message to the Service (in another process)
            try {
                teleMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // unbind or process might have crashes
            teleMessenger = null;
            isBound = false;
        }
    };
}
