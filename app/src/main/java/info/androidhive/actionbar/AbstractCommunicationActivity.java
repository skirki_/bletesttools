package info.androidhive.actionbar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import info.androidhive.ble.BleService;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.systemevents.PhoneIntentReceiver;
import info.androidhive.systemevents.SMSBroadcastReceiver;

/**
 * Created by hjacker on 31.05.15.
 */
public abstract class AbstractCommunicationActivity extends Activity {

    protected PhoneIntentReceiver phoneIntentReceiver;
    protected SMSBroadcastReceiver smsBroadcastReceiver;

    //Abstract fields
    private static Context context;

    private Handler mHandler = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        doBindService();
    }

    boolean isBound = false;
    Messenger mMessenger;

    void doBindService() {
        Intent intent = new Intent(this, BleService.class);
        bindService(intent, bleServiceConnection, Context.BIND_AUTO_CREATE);
    }

    void doUnbindService() {
        if (isBound) {
            // Detach our existing connection.
            unbindService(bleServiceConnection);
            isBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
 //       unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
        deactivateReceivers();
    }

    private ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            mMessenger = new Messenger(service);

            Toast.makeText(context, R.string.ble_service_connected, Toast.LENGTH_SHORT).show();

            mHandler = new Handler();

            onBleServiceConnected();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(context, R.string.local_service_disconnected, Toast.LENGTH_SHORT).show();
            // unbind or process might have crashes
            mMessenger = null;
            isBound = false;
        }
    };

    protected BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String action = bundle.getString(BleServiceIPC_Messages.ACTION);
                int resultCode = bundle.getInt(BleServiceIPC_Messages.RESULT);
                if(action==null) return;
                if(resultCode == AppMessages.BLUETOOTH_NOT_ENABLED) {
                    onBluetoothNotEnabled();
                    return;
                }

                if(AppMessages.ACTION_TAP_RECEIVED.equals(action)){
                    byte [] f = (byte [])bundle.get(BleServiceIPC_Messages.BYTE_ARRAY);
                    onTapReceived(f);
                }
                if(AppMessages.ACTION_RTC_DATA_READ.equals(action)){
                    byte [] f = (byte [])bundle.get(BleServiceIPC_Messages.BYTE_ARRAY);
                    onRtcDataRead(f);
                }

            }
        }
    };

    public void writeToTargetCharacteristic(byte [] payload){

        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_WRITE_CHARACTERISTIC_TO_DEVICE, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putSerializable(BleServiceIPC_Messages.BYTE_ARRAY, payload);
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    void onBluetoothNotEnabled(){

    }

    protected void deactivateReceivers(){
        if(phoneIntentReceiver != null)
            phoneIntentReceiver.disable();

        if(smsBroadcastReceiver != null)
        smsBroadcastReceiver.disable();
    }

    protected void activeReceivers(){
        if(phoneIntentReceiver != null)
            phoneIntentReceiver.setActive();
        if(smsBroadcastReceiver != null)
            smsBroadcastReceiver.setActive();
    }

    abstract void onBleServiceConnected();

    abstract void onBleServiceDisconnected();

    abstract void onReturnFromActivity();

    abstract void onTapReceived(byte [] f);

    abstract void onRtcDriverRead(byte [] f);

    abstract void onRtcDataRead(byte [] f);

    abstract void onAccDataRead(byte [] f);

    abstract void onAccDriverRead(byte [] f);
}
