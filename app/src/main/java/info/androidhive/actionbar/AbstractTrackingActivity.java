package info.androidhive.actionbar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.ble.BleService;
import info.androidhive.communication.AppMessages;
import info.androidhive.communication.BleServiceIPC_Messages;
import info.androidhive.functional.BleDevice;


public abstract class AbstractTrackingActivity extends Activity implements Refreshable{

    public static long RefreshRateInMs = 1000l;
    public final static String RSSI_VALUE = "info.androidhive.actionbar.rssi_value";

    public static final int RSSI_BUFFER_SIZE = 4;

    //Abstract fields
    private static Context context;

    private boolean mIsBound = false;

    private static Boolean isTrackingState = true;

    private final List<Integer> results = new ArrayList<Integer>();

    private Integer sum = 0;

    private Handler mHandler = null;

    //Fields To share
    protected Runnable mStatusChecker = null;

    protected synchronized void setToTracingState(){
        isTrackingState = true;
    }

    protected synchronized void escapeFromTracingMode(){
        isTrackingState = false;
    }

    protected synchronized void setRefreshRateInMs(long ms){
        RefreshRateInMs = ms;
    }

    protected synchronized void resetRefreshRateInMs(long ms){
        RefreshRateInMs = ms;
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        doBindService();
        setToTracingState();
    }

    boolean isBound = false;
    Messenger mMessenger;

    void doBindService() {
        Intent intent = new Intent(this, BleService.class);
        bindService(intent, bleServiceConnection, Context.BIND_AUTO_CREATE);
    }

    void doUnbindService() {
        if (isBound) {
            // Detach our existing connection.
            unbindService(bleServiceConnection);
            isBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    private ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            isBound = true;

            // Create the Messenger object
            mMessenger = new Messenger(service);

            Toast.makeText(context, R.string.ble_service_connected, Toast.LENGTH_SHORT).show();

            mHandler = new Handler();

            onBleServiceConnected();

            startSearch();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(context, R.string.local_service_disconnected, Toast.LENGTH_SHORT).show();
            // unbind or process might have crashes
            mMessenger = null;
            isBound = false;
        }
    };


    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    private void startSearch(){
        recurrentScan();
    }

    private synchronized void recurrentScan(){

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                filterResult(null);
                if (isTrackingState)
                    recurrentScan();
            }
        }, RefreshRateInMs);
    }

    public synchronized void filterResult(final BleDevice device){
        requestUpdateTargetRSSI();
    }

    @Override
    public void refresh(final Map<String, Object> args) {

        if(args.get(RSSI_VALUE)==null)
            return;
        Integer rssi_val = (Integer)args.get(RSSI_VALUE);

        if(rssi_val==null){ return;}

        if(results.size() > RSSI_BUFFER_SIZE){
            sum -= results.get(0);
            results.remove(0);
        }

        results.add(rssi_val);
        sum += rssi_val;

        final String rssi = args.get(RSSI_VALUE).toString();
        final Integer rssiVal = Integer.parseInt(rssi);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Double val = (double) sum / (double) results.size();
                onRssiRefresh(rssiVal, val);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if( keyCode == KeyEvent.KEYCODE_BACK ) {

            escapeFromTracingMode();
            onReturnFromActivity();
            return super.onKeyDown(keyCode, event);
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public void requestUpdateTargetRSSI(){
        Message msg = Message.obtain(null, BleServiceIPC_Messages.MSG_UPDATE_TARGET_RSSI, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putString(BleServiceIPC_Messages.RSSI, "0");
        msg.setData(bundle);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String action = bundle.getString(BleServiceIPC_Messages.ACTION);
                int resultCode = bundle.getInt(BleServiceIPC_Messages.RESULT);
                if(action==null) return;

                if(AppMessages.BLE_RSSI_UPDATED.equals(action)){
                    Integer rssi = (Integer)bundle.get(BleServiceIPC_Messages.OBJECT);
                    onRsiiUpdate(rssi);
                }

            }
        }
    };

    public void onRsiiUpdate(Integer rssi){
        Map<String, Object> result = new HashMap<>();

        if(rssi==null){ return;}

        result.put(RSSI_VALUE, rssi);

        refresh(result);
    }

    abstract void onRssiRefresh(Integer rssi, Double avgRssi);

    abstract void onBleServiceConnected();

    abstract void onBleServiceDisconnected();

    abstract void onReturnFromActivity();
}
