package info.androidhive.actionbar;

import java.util.List;

import info.androidhive.functional.BleDevice;

/**
 * Created by hjacker on 30.05.15.
 */
public interface BleBroadcast {

    /**
     * Broadcast when Scan Start
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.SCAN_START);
     */
    void doBroadcastScanInProgress();

    /**
     * Broadcast when scan is done and return scanned devices list
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.SCAN_START);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)discoveredDevices
     *
     * @param discoveredDevices #List of #BleDevice Objects
     */
    void doBroadcastScanDone(List<BleDevice> discoveredDevices);

    /**
     * Broadcast when Bluetooth in device is enable as "everything is ok" confirmation
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     */
    void doBroadcastBleEnable();

    /**
     * Broadcast when Bluetooth in device is not enable and is need to turn it on
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_NOT_ENABLED);
     */
    void doBroadcastBleNotEnable();

    /**
     * Broadcast when user is trying to connect to wrong device (that didn't contain target charact)
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.CONNECTION_TO_NOT_VALID_DEVICE);
     */
    void doBroadcastConnectionToNonValidDevice();

    /**
     * Broadcast when user is connected to device with @deviceName
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.CONNECTION_TO_NOT_VALID_DEVICE);
     *
     * @param deviceName name of connected device
     */
    void doBroadcastSuccessfulConnectionToDevice(String deviceName);

    /**
     * Broadcast when device with @deviceName was disconnected
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.BLE_DEVICE_DISCONNECTED);
     *
     * @param deviceName name of connected device
     */
    void doBroadcastDeviceDisconnected(String deviceName);

    /**
     * Broadcast when new RSSI value was read
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.BLE_RSSI_UPDATED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(Integer)myDevice.getLatestRssi()
     *
     * @param RSSI value of #rssi read from connected device
     */
    void odBroadcastOnRssiUpdate(Integer RSSI);

    /**
     * Broadcast when incoming frame was TAP Detected event
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_TAP_RECEIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)TAP //TODO: create
     */
    void doBroadcastOnTapDetected(byte [] f);
    //same as above but dedicated for triple tap
    void doBroadcastOnTripleTapDetected(byte [] f);
    /**
     * Broadcast when incoming frame was PEDOMETER_DATA Read event, put pedometer data bytes into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_ACC_DATA_RECIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(byte [])bytes
     *
     * @param bytes pedometer data bytes
     */
    void doBroadcastOnReadPedometerData(byte [] bytes);

    /**
     * Broadcast when incoming frame was PEDOMETER_DRIVER Read event, put pedometer driver bytes into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_ACC_DRIVER_RECIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(byte [])bytes
     *
     * @param bytes pedometer data bytes
     */
    void doBroadcastOnReadPedometerDriver(byte [] bytes);

    /**
     * Broadcast when incoming frame was BLE Driver Read event, put ble driver bytes into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_BLE_DRIVER_RECIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(byte [])bytes
     *
     * @param bytes pedometer data bytes
     */
    void doBroadcastOnReadBleDriver(byte [] bytes);

    /**
     * Broadcast when incoming frame was RTC Driver Read event, put rtc driver bytes into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_RTC_DRIVER_RECIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(byte [])bytes
     *
     * @param bytes pedometer data bytes
     */
    void doBroadcastOnReadRtcDriver(byte [] bytes);

    /**
     * Broadcast when incoming frame was RTC Data Read event, put rtc driver bytes into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_RTC_DATA_RECIVED);
     *      Objects:
     *      - BleServiceIPC_Messages.OBJECT = (Serializable)(byte [])bytes
     *
     * @param bytes pedometer data bytes
     */
    void doBroadcastOnReadRtcData(byte [] bytes);

    /**
     * Broadcast when incoming frame was Flash memory data, put bytes from flash into extras
     *
     * Intent = BleServiceIPC_Messages.NOTIFICATION_CODE
     * Extras:
     *      - BleServiceIPC_Messages.RESULT = AppMessages.BLUETOOTH_ENABLED);
     *      - BleServiceIPC_Messages.ACTION = AppMessages.ACTION_FLASH_DATA_READ);
     *      Objects:
     *      - BleServiceIPC_Messages.BYTE_ARRAY = (Serializable)(byte [])bytes
     *
     * @param bytes flash data bytes
     */
    void doBroadcastOnFlashMemoryData(byte [] bytes);
}
