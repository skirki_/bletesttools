package info.androidhive.actionbar;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TAP_Activity_9 extends AbstractCommunicationActivity {

    private int tap_counter = 0;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action9);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);

        doBindService();
        configurePushButtons();
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived(byte [] f) {
        onTapRecive();
    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {

    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

    private void configurePushButtons() {
        configureClearButton();
    }

    private void increaseTapCounter(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tap_counter++;

                final TextView allTap = (TextView) findViewById(R.id.textView45);

                allTap.setText(": " + tap_counter);
            }
        });

    }

    private void refreshTapAxis(final int i){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TextView allTap = (TextView) findViewById(R.id.textView48);

                allTap.setText(" " + i);
            }
        });

    }

    private void clearTapCounter(){
        this.tap_counter = 0;
        final TextView allTap = (TextView) findViewById(R.id.textView48);
        allTap.setText(": " + tap_counter);
    }

    private void configureClearButton(){
        final Button button = (Button) findViewById(R.id.button10);
        final TextView latestTap = (TextView) findViewById(R.id.textView45);
        final TextView allTap = (TextView) findViewById(R.id.textView48);
        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                tap_counter = 0;
                latestTap.setText("    --- ");
                allTap.setText("    --- ");
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if( keyCode == KeyEvent.KEYCODE_BACK ) {
            return super.onKeyDown(keyCode, event);
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void onTapRecive() {
        increaseTapCounter();
    }
}
