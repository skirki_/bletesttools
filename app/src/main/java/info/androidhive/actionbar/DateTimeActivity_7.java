package info.androidhive.actionbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import info.androidhive.communication.BleServiceIPC_Messages;

public class DateTimeActivity_7 extends AbstractCommunicationActivity {

    private static Context context;
    private boolean mIsBound = false;
    private TextView displayTime;

    static final int TIME_DIALOG_ID = 0;

    private int pHour;
    private int pMinute;
    Calendar myCalendar = Calendar.getInstance();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action7);
        TextView text = (TextView) findViewById(R.id.NotificationTitle);
        context = this;

        doBindService();

        configurePushButtons();

        displayTime = (TextView) findViewById(R.id.digiClock);
        registerReceiver(receiver, new IntentFilter(BleServiceIPC_Messages.NOTIFICATION));
    }

    @Override
    void onBleServiceConnected() {

    }

    @Override
    void onBleServiceDisconnected() {

    }

    @Override
    void onReturnFromActivity() {

    }

    @Override
    void onTapReceived(byte [] f) {

    }

    @Override
    void onRtcDriverRead(byte [] f) {

    }

    @Override
    void onRtcDataRead(byte [] f) {
        byte [] response = f;

        if(response.length < 8) return;
        int offset = 5;
        final Map<String, Integer> dateTime = new HashMap<String, Integer>();
        int hours   = response[offset+0];
        int minutes = response[offset+1];
        int seconds = response[offset+2];
        int timeBase= response[offset+3];

        int weekDay = response[offset+4];
        int month   = response[offset+5];
        int date    = response[offset+6];
        int year    = response[offset+7];

        dateTime.put("hours", hours);
        dateTime.put("minutes", minutes);
        dateTime.put("seconds", seconds);
        dateTime.put("timeBase", timeBase);
        dateTime.put("weekDay", weekDay);
        dateTime.put("month", month);
        dateTime.put("date", date);
        dateTime.put("year", year);

        updateDateTime(dateTime);
    }

    @Override
    void onAccDataRead(byte [] f) {

    }

    @Override
    void onAccDriverRead(byte [] f) {

    }

    private void configurePushButtons() {
        configureRefreshButton();
        configureReadDateTimeButton();
        configureWriteDateButton();
        configureWriteTimeButton();
    }

    private void configureRefreshButton(){
        final Button button = (Button) findViewById(R.id.refreshDateTimeButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                int pagesToRead = 8;

                byte [] readPayload = ("STR "+pagesToRead+" 6 0\r\n").getBytes();

                writeToTargetCharacteristic(readPayload);
            }
        });
    }

    private void configureReadDateTimeButton(){
        final Button button = (Button) findViewById(R.id.readDateTimeButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                int pagesToRead = 8;

                byte [] readPayload = ("STR "+pagesToRead+" 6 0\r\n").getBytes();

                writeToTargetCharacteristic(readPayload);

            }
        });
    }

    private void configureWriteDateButton(){
        final Button button = (Button) findViewById(R.id.writeDateButton);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void configureWriteTimeButton(){
        final Button button = (Button) findViewById(R.id.writeTimeButton);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                /** Get the current time */
                final Calendar cal = Calendar.getInstance();
                pHour = cal.get(Calendar.HOUR_OF_DAY);
                pMinute = cal.get(Calendar.MINUTE);

                showDialog(TIME_DIALOG_ID);
            }
        });
    }

    private void updateDateTime(final Map<String, Integer> dateTime){

        Runnable updateDateTime = new Runnable() {
            @Override
            public void run() {

                final int hours   = dateTime.get("hours");
                final int minutes = dateTime.get("minutes");
                final int seconds = dateTime.get("seconds");
                final int timeBase= dateTime.get("timeBase");

                final int weekDay = dateTime.get("weekDay");
                final int month   = dateTime.get("month");
                final int date    = dateTime.get("date");
                final int year    = dateTime.get("year");

                TextView text = (TextView) findViewById(R.id.digiClock);
                TextView text2 = (TextView) findViewById(R.id.textView51);
                String tbc;
                if(timeBase == 0){
                    tbc = "PM";
                } else {
                    tbc = "AM";
                }

                text.setText(hours+":"+minutes+":"+seconds+":"+tbc);
                text2.setText(hours+":"+minutes+":"+seconds+":"+tbc);

                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                String datDescription = null;
                int day = convertStmToJavaDayOfWeek(weekDay);
                switch(day){
                    case 1 : datDescription = "Sun"; break;
                    case 2 : datDescription = "Mon"; break;
                    case 3 : datDescription = "Tue"; break;
                    case 4 : datDescription = "Wed"; break;
                    case 5 : datDescription = "Thu"; break;
                    case 6 : datDescription = "Fri"; break;
                    case 7 : datDescription = "Sat"; break;
                }

                final TextView textView = (TextView) findViewById(R.id.dateText);
                String displayDate = (month)+"/"+date+"/"+year + " -- " + datDescription;
                textView.setText(displayDate); // dateText
            }
        };


        runOnUiThread(updateDateTime);
    }

    private void writeTime(){
        int pagesToWrite = 1;
        String timeToWrite = (pHour + " " + pMinute +" 0 0 \r\n");
        byte [] writePayload = ("STW "+pagesToWrite*4+" 6 0 " + timeToWrite).getBytes();

        writeToTargetCharacteristic(writePayload);
    }

    /** Callback received when the user "picks" a time in the dialog */
    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    pHour = hourOfDay;
                    pMinute = minute;
                    updateDisplay();
                    displayToast();
                    writeTime();
                }
            };

    /** Displays a notification when the time is updated */
    private void displayToast() {
        Toast.makeText(this, new StringBuilder().append("Time choosen is ").append(displayTime.getText()),   Toast.LENGTH_SHORT).show();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this,
                        mTimeSetListener, pHour, pMinute, false);
        }
        return null;
    }

    /** Updates the time in the TextView */
    private void updateDisplay() {
        displayTime.setText(
                new StringBuilder()
                        .append(pad(pHour)).append(":")
                        .append(pad(pMinute)));
    }

    /** Add padding to numbers less than ten */
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            writeDate();
            updateLabel();
        }

    };



    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        final TextView textView = (TextView) findViewById(R.id.dateText);

        textView.setText(sdf.format(myCalendar.getTime())); // dateText
    }

    private void writeDate(){
        int pagesToWrite = 1;
        int startPage = 1;
        String space = " ";

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int dayOfMonth = myCalendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);

        dayOfWeek = convertJavaToStmDayOfWeek(dayOfWeek);

        String dateToWrite = ( dayOfWeek + space + (month+1) + space + dayOfMonth + space + (year-2000)  + "\r\n");
        byte [] writePayload = ("STW "+ pagesToWrite*4 +" 6 " + startPage + space + dateToWrite).getBytes();

        writeToTargetCharacteristic(writePayload);
    }

    Integer convertStmToJavaDayOfWeek(int nr){
        int dayOfWeek = nr;
        if(dayOfWeek == 7) dayOfWeek = 1;
        else dayOfWeek += 1;

        return dayOfWeek;
    }


    Integer convertJavaToStmDayOfWeek(int nr){
        int dayOfWeek = nr;
        if(dayOfWeek == 1) dayOfWeek = 7;
        else dayOfWeek -= 1;

        return dayOfWeek;
    }

    public void showFrame(final byte [] response) {
        if(response.length < 8) return;

        Runnable showFrame = new Runnable() {
            @Override
            public void run() {

                String space = " ";
                int hours   = response[0];
                int minutes = response[1];
                int seconds = response[2];
                int timeBase= response[3];

                int weekDay = response[4];
                int month   = response[5];
                int date    = response[6];
                int year    = response[7];

                String framePayload = hours + space + minutes + space + seconds + space +
                        timeBase + space + space + weekDay + space + month +
                        space + date + space + year;

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Your frame payload: \r\n " + framePayload);
                builder1.setCancelable(true);
                builder1.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        };


        runOnUiThread(showFrame);
    }
}
