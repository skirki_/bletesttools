package info.androidhive.actionbar;


import java.util.Map;

public class ResponseContent implements Refreshable {

    public int bytesCounter = 0;

    protected byte [] content;

    protected Runnable responseAction;

    public Runnable getResponseAction() {
        return responseAction;
    }

    public void setResponseAction(Runnable responseAction) {
        this.responseAction = responseAction;
    }

    public byte[] getContent() {
        return content;
    }

    public void setResponseContent(byte[] byteContent){

        if(bytesCounter >= 0){

            if(content != null){
                byte[] one = byteContent;
                byte[] two = content;
                byte[] combined = new byte[one.length + two.length];


                System.arraycopy(two, 0, combined, 0,          two.length);
                System.arraycopy(one, 0, combined, two.length, one.length);

                content = combined;
            } else {
                content =  java.util.Arrays.copyOf(byteContent, byteContent.length);
            }

            bytesCounter -= byteContent.length;
        }

    }

    @Override
    public void refresh(Map<String, Object> args) {

        responseAction.run();

    }

    public ResponseContent (){}

    public ResponseContent(int bytesCounter){
        this.bytesCounter = bytesCounter;
    }

    public boolean isDone(){
        if(bytesCounter > 0)
            return false;
        else
            return true;
    }
}
