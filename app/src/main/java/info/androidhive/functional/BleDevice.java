package info.androidhive.functional;


import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import info.androidhive.parser.AdElement;
import info.androidhive.parser.AdParser;

public class BleDevice implements Serializable {

    private final String deviceName;
    private final String vendor;
    private final String macAddress;
    private transient BluetoothDevice device;
    private Integer latestRssi;

    private List<BluetoothGattService> bleServices;

    //Specific features
    private Boolean isInDataMode = false;

    private BluetoothGattCharacteristic trackingCharacteristic;

    public BluetoothDevice getDevice(){
        return device;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getVendor() {
        return vendor;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public BleDevice(String deviceName, String vendor, String macAddress, BluetoothDevice bluetoothDevice, Integer rssi){
        this.deviceName = deviceName;
        this.vendor = vendor;
        this.macAddress = macAddress;
        this.device = bluetoothDevice;
        this.latestRssi = rssi;
    }

    public BleDevice(final BluetoothDevice device, final int rssi, final byte[] scanRecord){
        latestRssi = rssi;
        String deviceName = device.getName();
        StringBuffer b = new StringBuffer();
        int byteCtr = 0;
        for( int i = 0 ; i < scanRecord.length ; ++i ) {
            if( byteCtr > 0 )
                b.append( " ");
            b.append( Integer.toHexString( ((int)scanRecord[i]) & 0xFF));
            ++byteCtr;
            if( byteCtr == 8 ) {
                byteCtr = 0;
                b = new StringBuffer();
            }
        }
        ArrayList<AdElement> ads = AdParser.parseAdData(scanRecord);
        StringBuffer sb = new StringBuffer();
        for( int i = 0 ; i < ads.size() ; ++i ) {
            AdElement e = ads.get(i);
            if( i > 0 )
                sb.append(" ; ");
            sb.append(e.toString());
        }
        String additionalData = new String( sb );

        this.deviceName = deviceName;
        this.vendor = additionalData;
        this.macAddress = device.getAddress();
        this.device = device;
    }

    public void setBleServices(List<BluetoothGattService> services){
        this.bleServices = services;
    }

    public List<BluetoothGattService> getBleServices(){
        return bleServices;
    }

    public Boolean isInDataMode() {
        return isInDataMode;
    }

    public void setDataMode(Boolean isInDataMode) {
        this.isInDataMode = isInDataMode;
    }

    public BluetoothGattCharacteristic getTrackingCharacteristic() {
        return trackingCharacteristic;
    }

    public void setTrackingCharacteristic(BluetoothGattCharacteristic trackingCharacteristic) {
        this.trackingCharacteristic = trackingCharacteristic;
    }

    public Integer getLatestRssi() {
        return latestRssi;
    }

    public void setDevice(BluetoothDevice device){
        this.device = device;
    }

    public void updateRSSIvalue(Integer rssi){
        this.latestRssi = rssi;
    }
}
