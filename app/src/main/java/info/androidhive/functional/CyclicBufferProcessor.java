package info.androidhive.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import info.androidhive.actionbar.BleBroadcast;
import info.androidhive.ble.BytesBufferRing;

/**
 * Created by hjacker on 13.05.15.
 */
public class CyclicBufferProcessor implements BytesBufferRing {

    private static final byte [] FRAME = {(byte)0xff, 0x02, (byte)0xDA, (byte)0xBA, (byte)0x6a, (byte)0xd, (byte)0xa };
    private static final int SGN_IDX = 0;
    private static final int L_IDX = 1;
    private static final int CRC_IDX = L_IDX+1+FRAME[L_IDX];
    private static final int CR_IDX = L_IDX+1+FRAME[L_IDX]+1;
    private static final int LF_IDX = CR_IDX+1;

    private static final byte TEST_MARKER = 0;
    private static final byte EVENT_MARKER = 1;
    private static final byte WRITE_MARKET = 2;
    private static final byte READ_MARKET = 3;

    private static final byte ACC_DRIVER_FILE = 1;
    private static final byte ACC_DATA_FILE = 2;
    private static final byte BLE_DRIVER = 3;
    private static final byte RTC_DRIVER = 5;
    private static final byte RTC_DATA = 6;
    private static final byte FLASH_DATA = 7;


    private final static int BUFFER_SIZE = 128;
    private volatile AtomicInteger bufferTail = new AtomicInteger(0);
    private volatile AtomicInteger bufferHead = new AtomicInteger(0);

    private final byte [] byteRing = new byte[BUFFER_SIZE];

    private List<byte []> framesStack = new ArrayList<>();

    BleBroadcast broadcaster;

    public CyclicBufferProcessor (BleBroadcast bCaster){
        broadcaster = bCaster;
    }

    public MessageProcessor processor = new MessageProcessor();

    @Override
    public void processInput(byte[] buffer, int size) {
        final int startIndex = bufferTail.get();
        bufferTail.addAndGet(size);

        if(bufferTail.get() >= BUFFER_SIZE){
            //calculate valid value after reach end of buffer
            final int new_value = bufferTail.get() % BUFFER_SIZE;
            bufferTail.set( new_value );
        }

        updateBuffer(buffer, startIndex, startIndex + buffer.length);

        if(!framesStack.isEmpty()){
            processStackFrame();
        }

        if(bufferHead.get() >= BUFFER_SIZE){
            final int new_value = bufferHead.get() % BUFFER_SIZE;
            bufferHead.set( new_value );
        }
    }

    /**
     * In short update @byteRing, this method know that @byteRing is a circural buffer
     * so it override old values and is "Out Of The Bound" safty
     * the index automagically turned to zero-start when reach end of array
     *
     * @param buffer
     * @param start
     * @param end
     */
    public void updateBuffer(final byte [] buffer, final int start, final int end){
        int length = end-start;
        for(int i=0; i<length; i++){
            byteRing[(start+i) % BUFFER_SIZE] = buffer[i];
        }

        checkAndGetFrameIfAvailable();
    }

    private int calculateFrameSize(int dataSize){
        return dataSize+5;
    }

    private void checkAndGetFrameIfAvailable() {

        final int start = bufferHead.get()% BUFFER_SIZE; //BUG!! When start == 127!!

        //1. Check if frame start from 0xff
        boolean ff = byteRing[start]==(byte)0xff;
        if(! ff) return;

        //2. Get data from Frame length
        int fLength = byteRing[start+1];
        int fStart = start+2;

        //3a. check if from tail to head are fLength
        int fAbsLength = fLength+4;
        if( bufferTail.get()%BUFFER_SIZE != (fAbsLength + bufferHead.get() + 1 )%BUFFER_SIZE )
        return;

        //3. Check if frame is ended with <CR><LF>
        boolean completeFrame = (byteRing[(fStart+fLength+1)%BUFFER_SIZE]==FRAME[CR_IDX]  //(byte)0x0d
                                && byteRing[(fStart+fLength+2)%BUFFER_SIZE]==FRAME[LF_IDX]); //(byte)0x0a);
        if(! completeFrame) return;

        int sum = 0;
        byte crc = byteRing[(fStart+fLength)%BUFFER_SIZE];

        for(int i=0; i<fLength; i++){
            sum += byteRing[(fStart+i)%BUFFER_SIZE];
        }

        //4. check CRC
        if((sum + crc) % (byte)0xff != 0)
            return; //no valid CRC - data corruption

        bufferHead.addAndGet(calculateFrameSize(fLength));
        byte [] frame = new byte[fLength+2];
        if((start+fLength+2) < BUFFER_SIZE){
            System.arraycopy(byteRing, start, frame, 0, fLength + 2);
        } else {
            int sizeToEnd = BUFFER_SIZE - start;
            //split array from two parts
            System.arraycopy(byteRing, start, frame, 0, sizeToEnd);
            System.arraycopy(byteRing, 0, frame, sizeToEnd, (fLength + 2-sizeToEnd) ) ;
        }

        pushBackFrame(frame);
    }

    private void pushBackFrame(byte [] frame){
        framesStack.add(frame);
    }

    @Override
    public int length() {
        return bufferTail.get();
    }


    public byte[] pop() {
        if(framesStack.isEmpty())
            return null;

        byte [] result = framesStack.remove(0);
        return result;
    }

    public int stackSize(){
        return framesStack.size();
    }

    public void processStackFrame(){
        if(broadcaster == null) return;

        byte[] frame = pop();
        byte marker = frame[2];

        switch(marker){
            case TEST_MARKER  : onPingTest(frame); break;
            case EVENT_MARKER : onIncomingEvent(frame); break;
            case WRITE_MARKET : onWriteEvent(frame); break;
            case READ_MARKET  : onReadEvent(frame); break;
        }

    }

    private void onPingTest(byte [] f){

    }

    private void onIncomingEvent(byte [] f){
        //STE
        if(f[3] == (byte)1){
            //TAP
            if(f[4] == 2){
                broadcaster.doBroadcastOnTapDetected(f);
            } else if (f[4] == 3){
                broadcaster.doBroadcastOnTripleTapDetected(f);
            }
        }
    }

    private void onWriteEvent(byte [] f){

    }

    private void onReadEvent(byte [] f){

        byte marker = f[4];
        switch(marker){
            case ACC_DRIVER_FILE    : onReadPedometerDriver(f); break;
            case ACC_DATA_FILE      : onReadPedometerData(f); break;
            case BLE_DRIVER         : onReadBleDriver(f); break;
            case RTC_DRIVER         : onReadRtcDriver(f); break;
            case RTC_DATA           : onReadRtcData(f); break;
            case FLASH_DATA          : onReadFlashMemory(f); break;
        }
    }

    private void onReadRtcData(byte[] f) {
        broadcaster.doBroadcastOnReadRtcData(f);
    }

    private void onReadRtcDriver(byte[] f) {

    }

    private void onReadPedometerData(byte [] f){
        broadcaster.doBroadcastOnReadPedometerData(f);
    }

    private void  onReadPedometerDriver(byte [] f){
        broadcaster.doBroadcastOnReadPedometerData(f);
    }

    private void onReadBleDriver(byte [] f) {

    }

    private void onReadFlashMemory(byte [] f){
        broadcaster.doBroadcastOnFlashMemoryData(f);
    }
}

