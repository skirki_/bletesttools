package tests.info.androidhive;

import android.test.InstrumentationTestCase;

/**
 * First test to make sure that tests are correctly configured.
 */
public class TestsPrecursor extends InstrumentationTestCase {

    public void test_firstTest() throws Exception {

        final int expected = 1;
        final int reality = 1;

        assertEquals(expected, reality);
    }


}
